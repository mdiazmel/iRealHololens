﻿
using CognitiveServices;
using HoloToolkit.Unity;
using System;

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR.WSA.WebCam;
using UnityEngine.Windows;

public class Hud : MonoBehaviour
{

    public Text InfoPanel;
    public Text DebugPanel;
    public RawImage img;
    public RawImage faceImg1;
    public RawImage faceImg2;
 
    public Text labelImg1;
    public Text labelImg2;

    public LayerMask m_raycastLayer;
    public GameObject m_annotationParent;
    public GameObject m_annotationTemplate;

    Resolution m_cameraResolution;
    



    PhotoCapture _photoCaptureObject = null;
    //IEnumerator coroutine;

    //public Matrix4x4 m_cameraToWorldMatrix;

    //public string _subscriptionKey = "19a98d2a04b84635be57de4c3ba7126d";
    string _computerVisionEndpoint = "titanic.paris.inria.fr:9099";
    
    public TextToSpeechManager textToSpeechManager;

    void Start()
    {

        if (InfoPanel == null)
            return;

        DebugPanel.text = "Connecting to titanic.paris.inria.fr";
        labelImg1.text = "ID: ";
        faceImg1.enabled = false;
        labelImg1.enabled = false;
        StartCoroutine(CoroLoop());
    }

    IEnumerator CoroLoop()
    {
		int secondsInterval = 1;
        /******/
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
        yield return new WaitForSeconds(1);
        
        /******/
        while (true) {
			AnalyzeScene();
			yield return new WaitForSeconds(secondsInterval);
		}
    }


    void OnPhotoCaptureCreated(PhotoCapture captureObject)
    {
        _photoCaptureObject = captureObject;

        m_cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).Last();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = m_cameraResolution.width;
        c.cameraResolutionHeight = m_cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.JPEG;

        captureObject.StartPhotoModeAsync(c, OnPhotoModeStarted);
        
    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        _photoCaptureObject.Dispose();
        _photoCaptureObject = null;
    }

    private void OnPhotoModeStarted(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            
            string filename = string.Format(@"photo.jpg");
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);

            //doing this to get formatted image
            //_photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);
                        
            Debug.Log("Photo Mode started");
            
           
        }
        else
        {
            Debug.Log("Say: Unable to start photo mode!");

        }
    }

    void OnCapturedPhotoToDisk(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            string filename = string.Format(@"terminator_analysis.jpg");
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);
            byte[] image = File.ReadAllBytes(filePath);
            GetTagsAndFaces(image);
            //ReadWords(image);
            Debug.Log("Ending Get tags and Faces function!");

        }
        else
        {
            Debug.Log("Lost Connection or not able to save image.");
            //Debug.Log("ABORT");
        }
        //_photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }


    // Update is called once per frame
    void Update()
    {
        if (IsTargetVisible())
        {
            UnityEngine.VR.WSA.HolographicSettings.SetFocusPointForFrame(gameObject.transform.position, -Camera.main.transform.forward);
        }
    }

    private bool IsTargetVisible()
    {
        // This will return true if the target's mesh is within the Main Camera's view frustums.
        Vector3 targetViewportPosition = Camera.main.WorldToViewportPoint(gameObject.transform.position);
        return (targetViewportPosition.x > 0.0 && targetViewportPosition.x < 1 &&
            targetViewportPosition.y > 0.0 && targetViewportPosition.y < 1 &&
            targetViewportPosition.z > 0);
    }


    void AnalyzeScene()
    {
        InfoPanel.text = "CALCULATION PENDING";
        string filename = string.Format(@"terminator_analysis.jpg");
        string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);

        //doing this to get formatted image
        //_photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);
        Debug.Log("Shooting photo!");
        //Take a picture
        _photoCaptureObject.TakePhotoAsync(delegate (PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
        {
            List<byte> buffer = new List<byte>();
            Matrix4x4 cameraToWorldMatrix;

            photoCaptureFrame.CopyRawImageDataIntoBuffer(buffer);

            //Check if we can receive the position where the photo was taken
            if (!photoCaptureFrame.TryGetCameraToWorldMatrix(out cameraToWorldMatrix))
            {
                return;
            }
            Debug.Log("Photo Captured");

            //Start a coroutine to handle the server request
            
            StartCoroutine(RunComputerVision(buffer.ToArray(), cameraToWorldMatrix));
        });

        //PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);

    }

    public void GetTagsAndFaces(byte[] image)
    {

        try
        {
            //coroutine = RunComputerVision(image);
            //StartCoroutine(coroutine);
            Debug.Log("Running CV server...");
        }
        catch (Exception)
        {

            InfoPanel.text = "DIAGNOSTIC\n**************\n\nGet Tags failed.";
            Debug.Log("ABORT");
        }
    }



    IEnumerator RunComputerVision(byte[] image, Matrix4x4 cameraToWorldMatrix)
    {
        /*var headers = new Dictionary<string, string>() {
            { "Ocp-Apim-Subscription-Key", _subscriptionKey },
            { "Content-Type", "application/octet-stream" }
        };*/

        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(image);
        img.texture = tex;
        

        var headers = new Dictionary<string, string>() {
            { "Content-Type", "application/octet-stream" }
        };

        InfoPanel.text = "Connecting to CV Server: ..." + _computerVisionEndpoint + "/n" +
           " Content-Type: " + headers["Content-Type"];

        WWW www = new WWW(_computerVisionEndpoint, image, headers);

        
        yield return www;

        while (!www.isDone)
            Debug.Log("Waiting for response to HTTP request...");

        List<string> tags = new List<string>();
        var jsonResults = www.text;
        var myObject = JsonUtility.FromJson<AnalysisResult>(jsonResults);
        /*
        foreach (var tag in myObject.tags)
        {
            tags.Add(tag.name + "...." + System.Convert.ToString(tag.confidence));
            
        }
        */


        //DebugPanel.text = jsonResults;
        //DebugPanel.text = "SCENE ANALYSIS\nActivate tags:\n***************\n\n" + string.Join("\n", tags.ToArray());

        int count = 1;
        List<string> faces = new List<string>();
        labelImg1.text = "ID: ";

        if (myObject != null)
        {
            foreach (var face in myObject.faces)
            {
                //int left = face.faceRectangle.height;
                //int top = face.faceRectangle.top;
                //faces.Add( face.gender + ": " + System.Convert.ToString(left) + System.Convert.ToString(top));
                int x = face.faceRectangle.left;
                int y = face.faceRectangle.top;
                int height = face.faceRectangle.height;
                int width = face.faceRectangle.width;
                faces.Add(string.Format("{0} {1} is scanned in: {2,3}, {3,3}, height: {4,3}, width: {5,3}", face.gender,
                    face.name, x, y, height, width));

                // Loading detected faces into the HUD
                int newy = tex.height - height - y;
                UnityEngine.Color[] pix = tex.GetPixels(x, newy, width, height);
                //UnityEngine.Color[] pix = tex.GetPixels(x, y, width, height, 0);
                Texture2D destTex = new Texture2D(width, height);
                destTex.SetPixels(pix);
                destTex.Apply();
                //w1 = destTex.width;
                //h1 = destTex.height;
                if (!labelImg1.enabled) { labelImg1.enabled = true; }
                    
                labelImg1.text += face.name;
                

                if (count == 1)
                {
                    faceImg1.enabled = true;
                    faceImg1.texture = destTex;

                }
                if (count == 2)
                {
                    faceImg2.texture = destTex;
                    faceImg2.enabled = true;
                    labelImg2.text += face.name;
                }

                count++;

            }
            Debug.Log("SCENE DESCRIPTION ON\nFACE DETECTION ON\n\n" + string.Join("\n", faces.ToArray()));

            /*Vector3 position = cameraToWorldMatrix.MultiplyPoint(Vector3.zero);
            Quaternion rotation = Quaternion.LookRotation(-cameraToWorldMatrix.GetColumn(2), cameraToWorldMatrix.GetColumn(1));
            Camera raycastCamera = this.gameObject.GetComponentInChildren<Camera>();
            raycastCamera.transform.position = position;
            raycastCamera.transform.rotation = rotation;

            DrawFrames(raycastCamera, myObject);
            */
            
        }
        else
        {
            Debug.Log("No face detected in last shoot");
            faceImg1.enabled = false;
            labelImg1.enabled = false;

        }
        //MemoryStream ms = new MemoryStream(image);
        //pictureBox1.Image = Image.FromStream(ms);
    }

    /// <summary>
    /// Draw the annotations in the scene
    /// </summary>
    void DrawFrames(Camera raycastCamera, AnalysisResult jsonResponse)
    {
        
        foreach (var face in jsonResponse.faces) 
        {
            Vector3 topLeft = CalcTopLeftVector(face);
            Vector3 topRight = CalcTopRightVector(face);
            Vector3 bottomRight = CalcBottomRightVector(face);
            Vector3 bottomLeft = CalcBottomLeftVector(face);
            Vector3 raycastPoint = (topLeft + topRight + bottomRight + bottomLeft) / 4;
            Ray ray = raycastCamera.ScreenPointToRay(raycastPoint);

            RaycastHit centerHit;
            if (Physics.Raycast(ray, out centerHit, 15.0f, m_raycastLayer))
            {
                Ray topLeftRay = raycastCamera.ScreenPointToRay(topLeft);
                Ray topRightRay = raycastCamera.ScreenPointToRay(topRight);
                Ray bottomLeftRay = raycastCamera.ScreenPointToRay(bottomLeft);

                float distance = centerHit.distance;
                float goScaleX = Vector3.Distance(topLeftRay.GetPoint(distance), topRightRay.GetPoint(distance));
                float goScaleY = Vector3.Distance(topLeftRay.GetPoint(distance), bottomLeftRay.GetPoint(distance));

                GameObject go = Instantiate(m_annotationTemplate) as GameObject;
                go.transform.SetParent(m_annotationParent.transform);
                go.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward, Vector3.up);
                go.transform.position = centerHit.point;
                go.transform.localScale = new Vector3(0.1f, goScaleX, goScaleY);
            }
        }
    }

    /// <summary>
    /// Create the top left vector
    /// </summary>
    Vector3 CalcTopLeftVector(Face node)
    {
        Vector3 vector;
        //JSONNode rect = node.faceRectangle.left;
        vector = new Vector3(node.faceRectangle.left, node.faceRectangle.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the top right vector
    /// </summary>
    Vector3 CalcTopRightVector(Face node)
    {
        Vector3 vector;
        
        vector = new Vector3(node.faceRectangle.left + node.faceRectangle.width, node.faceRectangle.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom right vector
    /// </summary>
    Vector3 CalcBottomRightVector(Face node)
    {
        Vector3 vector;
        vector = new Vector3(node.faceRectangle.left + node.faceRectangle.width, node.faceRectangle.top + node.faceRectangle.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom left vector
    /// </summary>
    Vector3 CalcBottomLeftVector(Face node)
    {
        Vector3 vector;
        vector = new Vector3(node.faceRectangle.left, node.faceRectangle.top + node.faceRectangle.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Scale the vector
    /// </summary>
    Vector3 ScaleVector(Vector3 vector)
    {
        float scaleX = (float)Screen.width / (float)m_cameraResolution.width;
        float scaleY = (float)Screen.height / (float)m_cameraResolution.height;

        return vector * Math.Max(scaleX, scaleY);
    }

}


