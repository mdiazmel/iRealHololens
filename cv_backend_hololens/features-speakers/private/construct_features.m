
for i=[1,3,4,5]

    fname = sprintf('05_0%d_facedets_track#faceklt_gt_feats_spkid.mat',i);
    load(fullfile('/meleze/data0/amiech/facedets',fname));
    
    gtid = [facedets.gtid];
    m = gtid > 1;
    facedets = facedets(m);

    episode = i;
    episode_name = sprintf('s05e0%d',episode);
    dumpfile = fullfile('/meleze/data0/amiech/buffy/',episode_name,'%06d.jpg');
    facedets = face_features(facedets,dumpfile);

    save(sprintf('facedets_ep%d.mat',episode),'facedets');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
feat = mean_descriptor_norm(facedets);

K1 = feat*transpose(feat);

dist = L2DIST(feat,feat);

m = mean(dist(:));

K2 = exp(-m*dist);

save(sprintf('kernels_ep%d.mat',episode),'K1','K2');

%}
