function [bbox name] = cv_face_recog(Im, bbox, convNet, face_feats, GT, mean_face)

if bbox 
	%select best face
	bbox = bbox(1,:);


        %det = cat(2,det,[6.0 2.1]);


	fprintf('VGG face recognition...\n');
	descriptors = face_descriptors(Im, bbox, mean_face, convNet);

	sim = descriptors'*face_feats;
	[scores,ind] = sort(sim,'descend');
	id = median([GT(ind(1:3))]);

	names = {'Antoine','Ignacio','Cheikh','Mauricio'};

	name = names{id};
else
   name = 'No Face';
end

end
