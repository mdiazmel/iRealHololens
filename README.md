# iReal : Implementing interactive scene understanding for a mixed reality device (Hololens project)


The goal of this project is to build a demonstration prototype of a smart 
personal assistant that sees and understands its surroundings to help the user 
navigate in unfamiliar environ- ments, recognize new people and operate never 
seen before devices. The assistant will be implemented on the Microsoft HoloLens 
mixed reality device and will integrate the latest research software for 
automatic visual recognition and scene understanding developed in the Inria 
Willow team.


## Technical description and repository organisation

The general architecture of this project  consists of two modules: 
the **front--end** application and the **back-end** module.
  
The back-end module has two components: **the computer 
vision sub--module** and a **REST web server**. 
The computer vision sub-module is developed in Matlab. It uses heavily the graphics 
processing units (GPUs) of a server machine.   
  
The current front--end application consists of 
a basic visualization interface that acquires image frames from the RGB Hololens
camera and transmits them to the server.   The Hololens application displays
he results obtained from the server into the head--up display (HUD) embedded on the device. 
The current front-end of the demonstrator uses only a  simple interface.
It's developed using `Unity` and `C\#`.


### Front-end module 

This module is developed on the [iReal](iReal/) folder. It contains a project in Unity
with all the assets and scripts necessaries to compile the code using Visual Studio.


### Back-end module

Developements of the back-end module are organised into the [webserver](webserver/) 
and the [cv_backend_hololens/](cv_backend_hololens/) folders.
The former contains all the networking functions to etablish communication with the device.
The latter one compile the functions used during the computer vision processing.
